#Artists

A feladatot Laravel Lumenben készítettem el. A megoldás során törekedtem az egyszerűségre és arra, hogy megfelelően el legyen választva egymástól az adatok feldolgozása és parse-olása. Azért választottam a konzolos feldolgozást, mert a feladat esetén ezt tartottam a leghatékonyabbnak.

A megoldásom során feltételeztem, hogy a bemeneti adatok formailag helyesek, a kimeneti fájl elérési útja pedig a futtató user által írható. 
## Telepítés
```./composer.phar install```
## Használat
Ezt a parancsot a projekt mappájában kell kiadni.

```
php artisan artists:from-file inputFile outputFile minimumCount?
``` 
```
inputFile a bemeneti fájl elérési útja
outputFile a kimeneti fájl elérési útja
minimumCount a minimum szám, amely az előfordulást jelenti. Alapértelmezetetten 50, nem kötelező paraméter
```
