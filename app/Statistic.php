<?php

namespace App;

class Statistic
{
    private $artists = [];
    private $pairs = [];

    public function __construct(array $data)
    {
        $pairs = [];
        foreach ($data as $row) {
            foreach ($row as $index => $artist) {
                if (!in_array($artist, $this->artists)) {
                    $this->artists[] = $artist;
                }
                if (empty($pairs[$artist])) {
                    $pairs[$artist] = [];
                }
                if (count($row) > 1) {
                    $temp = $row;
                    unset($temp[$index]);
                    $pairs[$artist] = array_merge($pairs[$artist], $temp);
                }
            }
        }
        $list = [];
        foreach ($pairs as $id => $artists) {
            foreach ($artists as $artistId) {
                $k1 = $artistId.'-'.$id;
                $k2 = $id.'-'.$artistId;
                if (empty($list[$k1]) || empty($list[$k2])) {
                    $list[$k1] = 1;
                } elseif (empty($list[$k2])) {
                    ++$list[$k1];
                } else {
                    ++$list[$k2];
                }
            }
        }

        foreach ($list as $key => $count) {
            if (empty($this->pairs[$count])) {
                $this->pairs[$count] = [];
            }
            $this->pairs[$count][] = $key;
        }
        krsort($this->pairs);
    }

    public function getList($count = 50): array
    {
        $data = [];
        foreach ($this->pairs as $key => $value) {
            if ($key >= $count) {
                $data[] = $value;
            }
        }

        return $data;
    }
}
