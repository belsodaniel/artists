<?php

namespace App\Console\Commands;

use App\Statistic;
use Illuminate\Console\Command;

class LoadArtistsFromFile extends Command
{
    public $signature = 'artists:from-file {input} {output} {count?}';

    public function handle()
    {
        $input = $this->argument('input');
        if (!is_file($input)) {
            $this->error('Input file not found!');

            return 1;
        }
        $data = file_get_contents($input);
        $this->info('Data loaded from: '.$input);
        $rows = [];
        foreach (explode("\n", $data) as $index => $row) {
            $columns = explode(',', $row);
            $artists = [];
            foreach ($columns as $artist) {
                if (strlen($artist) > 0) {
                    $artists[] = $artist;
                }
            }

            if (count($artists) > 0) {
                $rows[] = array_unique($artists);
            } else {
                $this->warn('Empty row founded in the file! Line number: '.($index + 1));
            }
        }
        if (0 == count($rows)) {
            $this->warn('Empty data!');

            return -2;
        }
        $this->info('Input file row count: '.count($rows));
        $stat = new Statistic($rows);
        if ($data = $stat->getList($this->argument('count'))) {
            $output = [];
            foreach ($data as $row) {
                foreach ($row as $pair) {
                    $output[] = implode(',', explode('-', $pair));
                }
            }
            $outputFile = $this->argument('output');
            file_put_contents($outputFile, implode("\n", $output));
            $this->info('The file is available: '.$outputFile);

            return 0;
        }
        $this->warn('Empty query!');

        return -1;
    }
}
